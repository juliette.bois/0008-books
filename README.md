# Search a book
TP réalisé dans le cadre du S4 spé. développement web (cours de Agustina Guarcia) en deuxième année de DUT MMI (Métiers du Multimédia et de l'Internet)

**Exercice API - Juliette Bois**

**Principe :** Recherche un livre par nom ou par auteur. 
Tu pourras ainsi trouver ses caractéristiques principales (auteur, genre, nombre de pages, note, résumé). 
Et si tu ne sais pas quoi lire, tu peux piocher un lire au hasard en fonction du mot clé tapé dans la barre de recherche ! ;-)

**API utilisée :** Google Books

 **Mise en place de :** 
* [ ]     Configuration webpack
* [ ]     Convention BEM
* [ ]     SCSS pour le style avec un pattern 3-1
* [ ]     Javascript en modules avec ES6
