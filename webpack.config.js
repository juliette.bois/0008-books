const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const dev = process.env.NODE_ENV === 'dev';

let cssLoaders = [
	{ loader: 'css-loader', options: { importLoaders: 1 } }
]

if (!dev) {
	cssLoaders.push({
		loader: 'postcss-loader',
		options: {
			plugins: (loader) => [
				require('autoprefixer')({
					browsers: ['last 2 versions']
				})
			]
		}
	})
}

let config = {
	mode: 'development',
	entry: ['./src/scss/main.scss', './src/js/index.js'],
	watch: dev,
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: dev ? '[name].js' : '[name].[chunkhash].js'
	},
	devtool: dev ? 'cheap-module-eval-source-map' : 'source-map',
	devServer: {
		contentBase: path.resolve(__dirname, './dist'),
		historyApiFallback: true,
		inline: true,
		overlay: true,
		open: true,
		hot: true
	},
	devtool: 'eval-source-map',
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'eslint-loader',
				}
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				}
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
          			fallback: 'style-loader',
          			use: [...cssLoaders, 'sass-loader']
        		})
			},
			{
				test: /\.(png|jpg|gif)$/i,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 8192,
							name: '[name].[hash:7].[ext]'
						}
					},
					{
						loader: 'img-loader',
						options: {
							enabled: !dev
						}
					}
				]
			},
			{
				test: /\.(svg)$/i,
				use: [
					{
						loader: 'file-loader',
					}
				]
			}
		]
	},
	plugins: [
		new ExtractTextPlugin({
			filename: dev ? '[name].css' : '[name].[hash:8].css',
			disable: dev
		}),
		new HtmlWebpackPlugin({
			template:'./src/index.html',
			filename: 'index.html'
		}),
	]
}

if(!dev) {
	config.plugins.push(new UglifyJsPlugin({
		sourceMap: true
	}))
	config.plugins.push(new ManifestPlugin())
}

module.exports = config;