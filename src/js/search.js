import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;
import Book from './books.js';
import Random from './random.js';

export default class Search {
	constructor () {
		this.initEls(); // méthode
		this.initEvents();
	}

	initEls () {
		//console.log('test');
	}

	initEvents () {
		this.searchForm();
		this.randomSearch();
	}

	searchForm() {
		const book = new Book();
		$('form').submit(function() {
			var search = $('input[name="books"]').val();
			book.getBooks(search);
			return false;
		});
	}

	randomSearch() {
		const random = new Random();
		$('button.random').click(function() {
			var search = $('input[name="books"]').val();
			if(!search) {
				var search = 'laravel';
			}
			random.getRandom(search);
		});
	}
}