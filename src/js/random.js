import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;
import Book from './books.js';
import Search from './search.js';

export default class Random {
	constructor () {
		this.initEls(); // méthode
		this.initEvents();
	}

	initEls () {
		//console.log('test');
	}

	initEvents () {

	}

	getRandom(search) {
		let getRandBook = (array) => {
			let length = array.length;
			let randIndex = Math.floor(Math.random() * length);
			return array[randIndex];
		};

		var axios = require('axios');
		axios.get('https://www.googleapis.com/books/v1/volumes?q='+search)
		.then(function (response) {
			let books_data = response.data;
			let books_array = [];
			books_data.items.forEach(element => {
				books_array.push(element);
			});
			let book_rand = getRandBook(books_array);
			console.log(book_rand);

			let title = book_rand.volumeInfo.title;
			let author = book_rand.volumeInfo.authors;
			let genre = book_rand.volumeInfo.categories;
			let pageCount = book_rand.volumeInfo.pageCount;
			let rating = book_rand.volumeInfo.averageRating;
			let summary = book_rand.volumeInfo.description;
			let thumbnail = book_rand.volumeInfo.imageLinks.thumbnail;
			
			$('span.books__about_title').text(title);
			$('span.books__about_author').text(author);
			$('span.books__about_genre').text(genre);
			$('span.books__about_page_count').text(pageCount);
			$('span.books__about_rating').text(rating);
			$('span.books__about_description').text(summary);
			$('img.books__picture_thumbnail').attr('src', thumbnail);
		})
		.catch(function (error) {
			console.log(error);
		});
	}
}