let getRandId = (array) => {
	let length = array.length;
	let randIndex = Math.floor(Math.random() * length);
	return array[randIndex];
};

var axios = require('axios');
axios.get('https://www.googleapis.com/books/v1/volumes?q=harry potter')
.then(function (response) {
	let book_data = response.data;
	let book_id = [];
	book_data.items.forEach(element => {
		book_id.push(element.id);
	});
	let idRand = getRandId(book_id);
	console.log(idRand);
  
})
.catch(function (error) {
	console.log(error);
});