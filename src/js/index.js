import '../scss/main.scss';
import Books from './books.js';
import Search from './search.js';
import Random from './random.js';

class App {
	constructor() {
		this.initApp();
	}

	initApp() {
		this.books = new Books();
		this.search = new Search();
		this.random = new Random();
	}
}

new App();