import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;
import Search from './search.js';
import Random from './random.js';

export default class Books {
	constructor () {
		this.initEls(); // méthode
		this.initEvents();
	}

	initEls () {
		//console.log('test');
	}

	initEvents () {
		this.getBooks('laravel');
	}

	getBooks(search) {
		$.ajax({
			'url': 'https://www.googleapis.com/books/v1/volumes?q='+search,
			'method': 'GET'
		}).done(function(response) {
			//console.log(response.items);
			$(response['items']).each(function() {
				let title = $(this)[0].volumeInfo.title;
				let author = $(this)[0].volumeInfo.authors;
				let genre = $(this)[0].volumeInfo.categories;
				let pageCount = $(this)[0].volumeInfo.pageCount;
				let rating = $(this)[0].volumeInfo.averageRating;
				let summary = $(this)[0].volumeInfo.description;
				let thumbnail = $(this)[0].volumeInfo.imageLinks.thumbnail;
				
				$('span.books__about_title').text(title);
				$('span.books__about_author').text(author);
				$('span.books__about_genre').text(genre);
				$('span.books__about_page_count').text(pageCount);
				$('span.books__about_rating').text(rating);
				$('span.books__about_description').text(summary);
				$('img.books__picture_thumbnail').attr('src', thumbnail);
			});
		});
	}
}